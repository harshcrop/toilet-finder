#### Introduction

App which uses google maps services to search for user nearest toilet.

##Install Instructions

* Run Yarn install
* $ npm run server & $ npm run react-app-start

## JSON FORMATE OF THE MONGO DB DATA

`name" : "demo",
    "address" : "Moti Bazaar Road, Palliviya Nagar, Parshvanath Nagar, Palanpur, Gujarate, Índia",
    "telephone" : "1234567890",
    "type" : "store",
    "description" : "demo",
    "location" : {
        "lat" : "24.172870",
        "lng" : "72.441978"
    }
`
#### Feats

* Express
* MongoDB with Mongoose
* React + Redux
* Google Maps Services (directions and location)
* Bulma CSS
* Font Awesome
* Gulp
* Redux Form with validations
* Alerts using SweetAlert
* ES6 everywhere!


