import React, { Component, PropTypes } from 'react';

const selectOptions = [
  { label: 'All distances', value: 0 },
  { label: 'Up to 5km', value: 5 },
  { label: 'Up to 10km', value: 10 },
  { label: 'Up to 25km', value: 25 },
  { label: 'Up to 50km', value: 50 },
  { label: 'Up to 100km', value: 100 }
]

class MapsFilters extends Component {
  static PropTypes = {
    initialValue: PropTypes.object.isRequired,
    filterSelect: PropTypes.func.isRequired
  }

  handleChangeSelect = (event) => {
    this.props.filterSelect(event.target.value);
  }

  render() {
    const { initialValue } = this.props;
    return (
      <div style={{ marginTop: 10, marginLeft: 10 }}>
         <label className="label">Maximum Distance</label>
          <p className="control center" >
            <span className="select">
              <select onChange={this.handleChangeSelect} defaultValue={initialValue}>
                {
                  selectOptions.map(option => {
                    return (<option key={option.value} value={option.value}>{option.label}</option>)
                  })
                }
              </select>
            </span>
          </p>
          <hr/>
      </div>
    );
  }
}

export default MapsFilters;
