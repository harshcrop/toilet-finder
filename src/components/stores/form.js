import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import _ from 'lodash';
import renderInputText from '../inputs/text';

import renderInputGeosuggest from '../inputs/geosuggest';


const validate = values => {
  const errors = {}
  const requiredFields = ['name', 'place', 'description'];

  _.each(requiredFields, field => {
    if (!values[field]) {
      errors[field] = 'This field is required';
    }
  });

  if (!values.address) {
    errors.place = 'Location not found for this address';
  }

  return errors;
}

class StoresForm extends Component {

  static propTypes = {
    initialValues: PropTypes.object,
    onSubmit: PropTypes.func.isRequired
  }

 
  onSuggestSelect = (suggest) => {
    if (suggest.location) {
      this.props.change('location', suggest.location);
      this.props.change('address', suggest.gmaps ? suggest.gmaps.formatted_address : suggest.label);
    }
  }

  onSuggestNoResults = () => {
    this.props.change('location', "");
    this.props.change('address', "");
  }

  formReset = () => {
    this.props.change('location', this.props.initialValues.location);
    this.props.reset();
  }

  render () {
    const { handleSubmit, submitting, initialValues, pristine } = this.props;



    return (
      <form className="is-horizontal" onSubmit={handleSubmit}>
        <Field name="location.lat" component="input" type="hidden" value={!initialValues ? "" : !initialValues.location ? "" : initialValues.location.lat}/>
        <Field name="location.lng" component="input" type="hidden" value={!initialValues ? "" : !initialValues.location ? "" : initialValues.location.lng}/>
        <Field name="address" component="input" type="hidden"/>

        <Field name="name" component={renderInputText} type="text" label="name:" />
        <Field name="place" component={renderInputGeosuggest} type="text" label="place:" onSuggestSelect={this.onSuggestSelect} onSuggestNoResults={this.onSuggestNoResults}/>
      
        
        <Field name="description" component={renderInputText} type="text" label="description:" />

        <button className="button is-primary" type="submit" disabled={submitting}>Submit</button>
        <button className="button is-link" type="button" disabled={pristine || submitting} onClick={this.formReset}>Restart</button>
      </form>
    );
  }
}


StoresForm = reduxForm({
  form: 'storesForm',
  validate
})(StoresForm)

export default connect()(StoresForm)
